package atr.atrui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("atr.atrui")
public class AtrUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtrUiApplication.class, args);
	}
}
