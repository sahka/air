var gulp = require('gulp'),
    browserify = require('browserify'),
    babelify = require("babelify"),
    rename = require('gulp-rename'),
    path = require('path'),
    stylus = require('gulp-stylus'),
    sourcemaps = require('gulp-sourcemaps'),
    //minifyCSS = require('gulp-minify-css'),
    nib = require('nib'),
    source = require("vinyl-source-stream"),
    buf = require("vinyl-buffer"),
    debowerify = require("debowerify"),
    shim = require("browserify-shim"),
    uglify = require("gulp-uglify"),
    watchify = require('watchify'),
    fromArgs = require('watchify/bin/args'),
    streamify = require('gulp-streamify'),
    inquirer = require("inquirer"),
    //browserSync = require('browser-sync'),
    es6ify = require('es6ify'),
    reactify = require('reactify');
var cssmin = require('gulp-cssmin');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var buffer = require('gulp-buffer');
var strip = require('gulp-strip-comments');



var config = {
    projects : {
        js: {
            filename: 'app.js',
            source: './ang/src/main/webapp/resources/dev/js/app.js',
            dest: './ang/src/main/webapp/resources/static/js/',
        },
        css: {
            filename: 'style.css',
            source: './ang/src/main/webapp/resources/dev/styl/style.styl',
            dest: './ang/src/main/webapp/resources/static/css/',

        }
    }
};


var handleErrors = function(type,data){
    console.log('!!! error -->',type,data);
    gutil.log.bind(data,'!!!');
}

gulp.task('set-prod-node-env', function() {
    return process.env.NODE_ENV = 'production';
});

gulp.task('projects.stylus',function(){
    gulp.watch(
            config.projects.css.source.replace('style.styl','')+'**/*.styl',
            function(event){
                gulp.run('system.projects.stylus-compile');
            }
    )
    gulp.run('system.projects.stylus-compile');
})

gulp.task('system.projects.stylus-compile', function(){
    return gulp.src(config.projects.css.source)
        .pipe(sourcemaps.init())
        .pipe(stylus({
            'include css': true,
            use: [nib()],
            compress: true,
            linenos: true,

        }))
        .on('error', function(res){
            handleErrors('stylus',res)
            this.emit('end')
        })
        .pipe(cssmin())
        .pipe(gulp.dest('build'))
        //.pipe(minifyCSS({keepBreaks:false}))
        .pipe(sourcemaps.write())
        .pipe(rename(config.projects.css.filename))
        .pipe(gulp.dest(config.projects.css.dest))

});


gulp.task('frontend.stylus',function(){
    gulp.watch(
            config.projects.css.source.replace('style.styl','')+'**/*.styl',
            function(event){
                gulp.run('system.frontend.stylus-compile');
            }
    )
    gulp.run('system.frontend.stylus-compile');
})

gulp.task('system.frontend.stylus-compile', function(){
    return gulp.src(config.projects.css.source)
        .pipe(sourcemaps.init())
        .pipe(stylus({
            'include css': true,
            compress: true,
            linenos: true,

            use: [nib()]
        }))
        .on('error', function(res){
            handleErrors('stylus',res)
            this.emit('end')
        })
        .pipe(cssmin())
        .pipe(gulp.dest('build'))
        .pipe(sourcemaps.write())
        .pipe(rename(config.projects.css.filename))
        .pipe(gulp.dest(config.projects.css.dest))

});



var opts = {
    entries: config.projects.js.source,
    cache: {},
    packegeCache: {},
    fullPaths: true,
    insertGlobals: true,
    plugin: [watchify]

};

var b = watchify(browserify(opts)).transform("babelify", {presets: ["react","es2015"]});

b.on('log', gutil.log);
//b_f.on('log', gutil.log);


gulp.task('prod-projects',['projects.build','projects.strip','system.projects.stylus-compile-prod'])
gulp.task('prod-frontend',['frontend.build','frontend.strip','system.frontend.stylus-compile-prod'])
gulp.task('prod',['set-prod-node-env','prod-projects','prod-frontend'])
gulp.task('dev',['system.projects.stylus-compile','system.frontend.stylus-compile'])








var bundle_projects = function(){
    return b.bundle()
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source(config.projects.js.source+config.projects.js.filename))
        .pipe(buf())
        .pipe(sourcemaps.init())
        .pipe(rename(config.projects.js.filename))
        .pipe(gulp.dest(config.projects.js.dest))
        .pipe(sourcemaps.write('.'))

}

gulp.task('projects.js',  function(){
    b.on('update', bundle_projects);
    bundle_projects()
})



gulp.task('frontend.js',  function(){
    b_f.on('update', bundle_frontend);
    bundle_frontend()
})


gulp.task('projects-work',['projects.js','projects.stylus'])


gulp.task('bild',['projects-work'])
