window.$ = window.jQuery  = require("jquery");
require("jquery.cookie");

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
    }
});