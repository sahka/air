require('./app_settings');


import React from 'react';
import ReactDom from 'react-dom';
import { Router, Route, Link } from 'react-router';
import { browserHistory } from 'react-router'

import * as Main from './main/'
ReactDom.render(
    <Router history={browserHistory}>
        <Route path='/' component={Main.Wrapper}/>
    </Router>
    , document.getElementById('app')
)

document.currentScript.parentNode.removeChild(document.currentScript)
